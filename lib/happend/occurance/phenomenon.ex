defmodule Happend.Occurance.Phenomenon do
  use Ecto.Schema
  import Ecto.Changeset

  schema "phenomenons" do
    field :count, :integer
    field :description, :string
    field :title, :string

    timestamps(type: :utc_datetime)
  end

  @doc false
  def changeset(phenomenon, attrs) do
    phenomenon
    |> cast(attrs, [:title, :description, :count])
    |> validate_required([:title, :description, :count])
  end
end

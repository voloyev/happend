defmodule Happend.Occurance do
  @moduledoc """
  The Occurance context.
  """

  import Ecto.Query, warn: false
  alias Happend.Repo

  alias Happend.Occurance.Phenomenon

  @doc """
  Returns the list of phenomenons.

  ## Examples

      iex> list_phenomenons()
      [%Phenomenon{}, ...]

  """
  def list_phenomenons do
    Repo.all(Phenomenon)
  end

  @doc """
  Gets a single phenomenon.

  Raises `Ecto.NoResultsError` if the Phenomenon does not exist.

  ## Examples

      iex> get_phenomenon!(123)
      %Phenomenon{}

      iex> get_phenomenon!(456)
      ** (Ecto.NoResultsError)

  """
  def get_phenomenon!(id), do: Repo.get!(Phenomenon, id)

  @doc """
  Creates a phenomenon.

  ## Examples

      iex> create_phenomenon(%{field: value})
      {:ok, %Phenomenon{}}

      iex> create_phenomenon(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def create_phenomenon(attrs \\ %{}) do
    %Phenomenon{}
    |> Phenomenon.changeset(attrs)
    |> Repo.insert()
  end

  @doc """
  Updates a phenomenon.

  ## Examples

      iex> update_phenomenon(phenomenon, %{field: new_value})
      {:ok, %Phenomenon{}}

      iex> update_phenomenon(phenomenon, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def update_phenomenon(%Phenomenon{} = phenomenon, attrs) do
    phenomenon
    |> Phenomenon.changeset(attrs)
    |> Repo.update()
  end

  @doc """
  Deletes a phenomenon.

  ## Examples

      iex> delete_phenomenon(phenomenon)
      {:ok, %Phenomenon{}}

      iex> delete_phenomenon(phenomenon)
      {:error, %Ecto.Changeset{}}

  """
  def delete_phenomenon(%Phenomenon{} = phenomenon) do
    Repo.delete(phenomenon)
  end

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking phenomenon changes.

  ## Examples

      iex> change_phenomenon(phenomenon)
      %Ecto.Changeset{data: %Phenomenon{}}

  """
  def change_phenomenon(%Phenomenon{} = phenomenon, attrs \\ %{}) do
    Phenomenon.changeset(phenomenon, attrs)
  end
end

defmodule Happend.Repo do
  use Ecto.Repo,
    otp_app: :happend,
    adapter: Ecto.Adapters.Postgres
end

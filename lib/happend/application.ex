defmodule Happend.Application do
  # See https://hexdocs.pm/elixir/Application.html
  # for more information on OTP Applications
  @moduledoc false

  use Application

  @impl true
  def start(_type, _args) do
    children = [
      HappendWeb.Telemetry,
      Happend.Repo,
      {DNSCluster, query: Application.get_env(:happend, :dns_cluster_query) || :ignore},
      {Phoenix.PubSub, name: Happend.PubSub},
      # Start the Finch HTTP client for sending emails
      {Finch, name: Happend.Finch},
      # Start a worker by calling: Happend.Worker.start_link(arg)
      # {Happend.Worker, arg},
      # Start to serve requests, typically the last entry
      HappendWeb.Endpoint
    ]

    # See https://hexdocs.pm/elixir/Supervisor.html
    # for other strategies and supported options
    opts = [strategy: :one_for_one, name: Happend.Supervisor]
    Supervisor.start_link(children, opts)
  end

  # Tell Phoenix to update the endpoint configuration
  # whenever the application is updated.
  @impl true
  def config_change(changed, _new, removed) do
    HappendWeb.Endpoint.config_change(changed, removed)
    :ok
  end
end

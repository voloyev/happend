defmodule Happend.Event do
  use Ecto.Schema
  import Ecto.Changeset

  schema "events" do
    field :title, :string
    field :occurance, :integer

    timestamps(type: :utc_datetime)
  end

  @doc false
  def changeset(event, attrs) do
    event
    |> cast(attrs, [:title, :occurance])
    |> validate_required([:title, :occurance])
    |> validate_length(:title, min: 2)
  end
end

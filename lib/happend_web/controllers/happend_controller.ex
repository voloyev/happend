defmodule HappendWeb.HappendController do
  use HappendWeb, :controller

  def index(conn, _params) do
    render(conn, :index)
  end

  def show(conn, %{"id" => happend_id} = params) do
    render(conn, :show, happend_id: happend_id)
  end
end

defmodule HappendWeb.HappendHTML do
  use HappendWeb, :html

  embed_templates "happend_html/*"
end

defmodule HappendWeb.PhenomenonHTML do
  use HappendWeb, :html

  embed_templates "phenomenon_html/*"

  @doc """
  Renders a phenomenon form.
  """
  attr :changeset, Ecto.Changeset, required: true
  attr :action, :string, required: true

  def phenomenon_form(assigns)
end

defmodule HappendWeb.PageHTML do
  use HappendWeb, :html

  embed_templates "page_html/*"
end

defmodule HappendWeb.PhenomenonController do
  use HappendWeb, :controller

  alias Happend.Occurance
  alias Happend.Occurance.Phenomenon

  def index(conn, _params) do
    phenomenons = Occurance.list_phenomenons()
    render(conn, :index, phenomenons: phenomenons)
  end

  def new(conn, _params) do
    changeset = Occurance.change_phenomenon(%Phenomenon{})
    render(conn, :new, changeset: changeset)
  end

  def create(conn, %{"phenomenon" => phenomenon_params}) do
    case Occurance.create_phenomenon(phenomenon_params) do
      {:ok, phenomenon} ->
        conn
        |> put_flash(:info, "Phenomenon created successfully.")
        |> redirect(to: ~p"/phenomenons/#{phenomenon}")

      {:error, %Ecto.Changeset{} = changeset} ->
        render(conn, :new, changeset: changeset)
    end
  end

  def show(conn, %{"id" => id}) do
    phenomenon = Occurance.get_phenomenon!(id)
    render(conn, :show, phenomenon: phenomenon)
  end

  def edit(conn, %{"id" => id}) do
    phenomenon = Occurance.get_phenomenon!(id)
    changeset = Occurance.change_phenomenon(phenomenon)
    render(conn, :edit, phenomenon: phenomenon, changeset: changeset)
  end

  def update(conn, %{"id" => id, "phenomenon" => phenomenon_params}) do
    phenomenon = Occurance.get_phenomenon!(id)

    case Occurance.update_phenomenon(phenomenon, phenomenon_params) do
      {:ok, phenomenon} ->
        conn
        |> put_flash(:info, "Phenomenon updated successfully.")
        |> redirect(to: ~p"/phenomenons/#{phenomenon}")

      {:error, %Ecto.Changeset{} = changeset} ->
        render(conn, :edit, phenomenon: phenomenon, changeset: changeset)
    end
  end

  def delete(conn, %{"id" => id}) do
    phenomenon = Occurance.get_phenomenon!(id)
    {:ok, _phenomenon} = Occurance.delete_phenomenon(phenomenon)

    conn
    |> put_flash(:info, "Phenomenon deleted successfully.")
    |> redirect(to: ~p"/phenomenons")
  end
end

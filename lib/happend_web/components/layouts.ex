defmodule HappendWeb.Layouts do
  use HappendWeb, :html

  embed_templates "layouts/*"
end

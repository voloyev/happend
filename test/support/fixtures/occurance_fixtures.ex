defmodule Happend.OccuranceFixtures do
  @moduledoc """
  This module defines test helpers for creating
  entities via the `Happend.Occurance` context.
  """

  @doc """
  Generate a phenomenon.
  """
  def phenomenon_fixture(attrs \\ %{}) do
    {:ok, phenomenon} =
      attrs
      |> Enum.into(%{
        count: 42,
        description: "some description",
        title: "some title"
      })
      |> Happend.Occurance.create_phenomenon()

    phenomenon
  end
end

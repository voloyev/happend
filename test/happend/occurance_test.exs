defmodule Happend.OccuranceTest do
  use Happend.DataCase

  alias Happend.Occurance

  describe "phenomenons" do
    alias Happend.Occurance.Phenomenon

    import Happend.OccuranceFixtures

    @invalid_attrs %{count: nil, description: nil, title: nil}

    test "list_phenomenons/0 returns all phenomenons" do
      phenomenon = phenomenon_fixture()
      assert Occurance.list_phenomenons() == [phenomenon]
    end

    test "get_phenomenon!/1 returns the phenomenon with given id" do
      phenomenon = phenomenon_fixture()
      assert Occurance.get_phenomenon!(phenomenon.id) == phenomenon
    end

    test "create_phenomenon/1 with valid data creates a phenomenon" do
      valid_attrs = %{count: 42, description: "some description", title: "some title"}

      assert {:ok, %Phenomenon{} = phenomenon} = Occurance.create_phenomenon(valid_attrs)
      assert phenomenon.count == 42
      assert phenomenon.description == "some description"
      assert phenomenon.title == "some title"
    end

    test "create_phenomenon/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Occurance.create_phenomenon(@invalid_attrs)
    end

    test "update_phenomenon/2 with valid data updates the phenomenon" do
      phenomenon = phenomenon_fixture()
      update_attrs = %{count: 43, description: "some updated description", title: "some updated title"}

      assert {:ok, %Phenomenon{} = phenomenon} = Occurance.update_phenomenon(phenomenon, update_attrs)
      assert phenomenon.count == 43
      assert phenomenon.description == "some updated description"
      assert phenomenon.title == "some updated title"
    end

    test "update_phenomenon/2 with invalid data returns error changeset" do
      phenomenon = phenomenon_fixture()
      assert {:error, %Ecto.Changeset{}} = Occurance.update_phenomenon(phenomenon, @invalid_attrs)
      assert phenomenon == Occurance.get_phenomenon!(phenomenon.id)
    end

    test "delete_phenomenon/1 deletes the phenomenon" do
      phenomenon = phenomenon_fixture()
      assert {:ok, %Phenomenon{}} = Occurance.delete_phenomenon(phenomenon)
      assert_raise Ecto.NoResultsError, fn -> Occurance.get_phenomenon!(phenomenon.id) end
    end

    test "change_phenomenon/1 returns a phenomenon changeset" do
      phenomenon = phenomenon_fixture()
      assert %Ecto.Changeset{} = Occurance.change_phenomenon(phenomenon)
    end
  end
end

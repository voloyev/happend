defmodule HappendWeb.PhenomenonControllerTest do
  use HappendWeb.ConnCase

  import Happend.OccuranceFixtures

  @create_attrs %{count: 42, description: "some description", title: "some title"}
  @update_attrs %{count: 43, description: "some updated description", title: "some updated title"}
  @invalid_attrs %{count: nil, description: nil, title: nil}

  describe "index" do
    test "lists all phenomenons", %{conn: conn} do
      conn = get(conn, ~p"/phenomenons")
      assert html_response(conn, 200) =~ "Listing Phenomenons"
    end
  end

  describe "new phenomenon" do
    test "renders form", %{conn: conn} do
      conn = get(conn, ~p"/phenomenons/new")
      assert html_response(conn, 200) =~ "New Phenomenon"
    end
  end

  describe "create phenomenon" do
    test "redirects to show when data is valid", %{conn: conn} do
      conn = post(conn, ~p"/phenomenons", phenomenon: @create_attrs)

      assert %{id: id} = redirected_params(conn)
      assert redirected_to(conn) == ~p"/phenomenons/#{id}"

      conn = get(conn, ~p"/phenomenons/#{id}")
      assert html_response(conn, 200) =~ "Phenomenon #{id}"
    end

    test "renders errors when data is invalid", %{conn: conn} do
      conn = post(conn, ~p"/phenomenons", phenomenon: @invalid_attrs)
      assert html_response(conn, 200) =~ "New Phenomenon"
    end
  end

  describe "edit phenomenon" do
    setup [:create_phenomenon]

    test "renders form for editing chosen phenomenon", %{conn: conn, phenomenon: phenomenon} do
      conn = get(conn, ~p"/phenomenons/#{phenomenon}/edit")
      assert html_response(conn, 200) =~ "Edit Phenomenon"
    end
  end

  describe "update phenomenon" do
    setup [:create_phenomenon]

    test "redirects when data is valid", %{conn: conn, phenomenon: phenomenon} do
      conn = put(conn, ~p"/phenomenons/#{phenomenon}", phenomenon: @update_attrs)
      assert redirected_to(conn) == ~p"/phenomenons/#{phenomenon}"

      conn = get(conn, ~p"/phenomenons/#{phenomenon}")
      assert html_response(conn, 200) =~ "some updated description"
    end

    test "renders errors when data is invalid", %{conn: conn, phenomenon: phenomenon} do
      conn = put(conn, ~p"/phenomenons/#{phenomenon}", phenomenon: @invalid_attrs)
      assert html_response(conn, 200) =~ "Edit Phenomenon"
    end
  end

  describe "delete phenomenon" do
    setup [:create_phenomenon]

    test "deletes chosen phenomenon", %{conn: conn, phenomenon: phenomenon} do
      conn = delete(conn, ~p"/phenomenons/#{phenomenon}")
      assert redirected_to(conn) == ~p"/phenomenons"

      assert_error_sent 404, fn ->
        get(conn, ~p"/phenomenons/#{phenomenon}")
      end
    end
  end

  defp create_phenomenon(_) do
    phenomenon = phenomenon_fixture()
    %{phenomenon: phenomenon}
  end
end

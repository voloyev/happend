defmodule Happend.Repo.Migrations.CreatePhenomenons do
  use Ecto.Migration

  def change do
    create table(:phenomenons) do
      add :title, :string
      add :description, :string
      add :count, :integer

      timestamps(type: :utc_datetime)
    end
  end
end

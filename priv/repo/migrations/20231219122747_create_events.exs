defmodule Happend.Repo.Migrations.CreateEvents do
  use Ecto.Migration

  def change do
    create table(:events) do
      add :title, :string
      add :occurance, :integer

      timestamps(type: :utc_datetime)
    end
  end
end
